pull_submodule:
	git submodule update --init --recursive

update_submodule:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:exam23/protos.git

run_script:
	./script/gen-proto.sh

migrate_up:
	migrate -path migrations/ -database postgres://azizbek:Azizbek@localhost:5432/post_service?sslmode=disable up

migrate_down:
	migrate -path migrations/ -database postgres://azizbek:Azizbek@localhost:5432/post_service down

migrate_force:
	migrate -path migrations/ -database postgres://azizbek:Azizbek@localhost:5432/post_service force 1
