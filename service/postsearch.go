package service

import (
	"context"

	"gitlab.com/exam23/post-service/genproto/customer"
	pb "gitlab.com/exam23/post-service/genproto/post"
	"gitlab.com/exam23/post-service/genproto/rewiew"
	"gitlab.com/exam23/post-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (p *PostService) GetAll(ctx context.Context, req *pb.Empty) (*pb.GetPosts, error) {
	posts, err := p.Storage.Post().GetAll()
	if err != nil {
		p.Logger.Error("Error while getting all posts", logger.Error(err))
		return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all the posts")
	}
	response := &pb.GetPosts{}
	for _, post := range posts {
		temp := &pb.GetPostInfo{
			Id:          post.Id,
			Title:       post.Title,
			Description: post.Description,
			Content:     post.Content,
			UpdatedAt:   post.UpdatedAt,
			CreatedAt:   post.CreatedAt,
		}
		temp.Medias, err = p.Storage.Post().GetMediasByPostId(pb.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting medias of posts", logger.Error(err))
			// return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all medias of the posts")

		}
		owner, err := p.Client.Customer().GetCustomerI(ctx, &customer.Id{Id: post.OwnerId})
		if err != nil {
			p.Logger.Error("Error while getting owner of posts", logger.Error(err))
			return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get user of the posts")
		}
		tempOwner := &pb.Customer{
			Id:          owner.Id,
			FirstName:   owner.FirstName,
			LastName:    owner.LastName,
			Bio:         owner.Bio,
			Email:       owner.Email,
			PhoneNumber: owner.PhoneNumber,
			UpdatedAt:   owner.UpdatedAt,
			CreatedAt:   owner.CreatedAt,
		}
		// fmt.Println(owner)
		for _, ad := range owner.Addresses {
			tempOwner.Addresses = append(tempOwner.Addresses, &pb.Address{
				OwnerId:    ad.OwnerId,
				District:   ad.District,
				Street:     ad.Street,
				HomeNumber: ad.HomeNumber,
				CreatedAt:  ad.CreatedAt,
				Id:         ad.Id,
				UpdatedAt:  ad.UpdatedAt,
			})
		}
		temp.Owner = tempOwner
		rewiews, err := p.Client.Rewiew().GetRewiewByPostId(ctx, &rewiew.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting post rewiews", logger.Error(err))
			// return nil, status.Error(codes.Internal, "Couldn't get the post")
			rewiews = &rewiew.GetRewiewsRes{}
		}
		for _, rew := range rewiews.Rewiews {
			temp.Rewiews = append(temp.Rewiews, &pb.RewiewRes{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
				CreatedAt:   rew.CreatedAt,
				UpdatedAt:   rew.UpdatedAt,
			})
		}
		temp.AvarageRewiew = uint32(rewiews.Rating)
		response.Posts = append(response.Posts, temp)
	}
	response.Count = int64(len(posts))
	return response, nil
}

func (p *PostService) GetTop10(ctx context.Context, req *pb.Empty) (*pb.GetPosts, error) {
	topIds, err := p.Client.Rewiew().GetTop10Postids(ctx, &rewiew.Empty{})
	if err != nil {
		p.Logger.Error("Error while getting top 10 post ids", logger.Error(err))
		return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all the post ids")
	}
	response := &pb.GetPosts{}
	response.Count = int64(len(topIds.Ids))
	for _, id := range topIds.Ids {
		res, err := p.Storage.Post().GetById(&pb.Id{Id: id})
		if err != nil {
			p.Logger.Error("Error while getting post info", logger.Error(err))
			return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't posts")
		}
		temp := &pb.GetPostInfo{
			Id:          res.Id,
			Title:       res.Title,
			Description: res.Description,
			Content:     res.Content,
			CreatedAt:   res.CreatedAt,
			UpdatedAt:   res.UpdatedAt,
		}
		temp.Medias, err = p.Storage.Post().GetMediasByPostId(pb.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting medias of posts", logger.Error(err))
			// return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all medias of the posts")

		}
		owner, err := p.Client.Customer().GetCustomerI(ctx, &customer.Id{Id: res.OwnerId})
		if err != nil {
			p.Logger.Error("Error while getting owner of posts", logger.Error(err))
			// return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get user of the posts")
			owner = &customer.Customer{}
		}
		tempOwner := &pb.Customer{
			Id:          owner.Id,
			FirstName:   owner.FirstName,
			LastName:    owner.LastName,
			Bio:         owner.Bio,
			Email:       owner.Email,
			PhoneNumber: owner.PhoneNumber,
			CreatedAt:   owner.CreatedAt,
			UpdatedAt:   owner.UpdatedAt,
		}
		for _, ad := range owner.Addresses {
			tempOwner.Addresses = append(tempOwner.Addresses, &pb.Address{
				Id:         ad.Id,
				OwnerId:    ad.OwnerId,
				District:   ad.District,
				Street:     ad.Street,
				HomeNumber: ad.HomeNumber,
				CreatedAt:  ad.CreatedAt,
				UpdatedAt:  ad.UpdatedAt,
			})
		}
		temp.Owner = tempOwner
		rewiews, err := p.Client.Rewiew().GetRewiewByPostId(ctx, &rewiew.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting post rewiews", logger.Error(err))
			// return nil, status.Error(codes.Internal, "Couldn't get the post")
			rewiews = &rewiew.GetRewiewsRes{}
		}
		temp.AvarageRewiew = uint32(rewiews.Rating)
		for _, rew := range rewiews.Rewiews {
			temp.Rewiews = append(temp.Rewiews, &pb.RewiewRes{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
				UpdatedAt:   rew.UpdatedAt,
				CreatedAt:   rew.CreatedAt,
			})
		}
		response.Posts = append(response.Posts, temp)
	}
	return response, nil
}

func (p *PostService) GetPostBySearchAndOrder(ctx context.Context, req *pb.GetListBySearchReq) (*pb.GetPosts, error) {
	posts, err := p.Storage.Post().GetPostBySearchAndOrder(*req)
	if err != nil {
		p.Logger.Error("Error while getting all posts", logger.Error(err))
		return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all the posts")
	}
	response := &pb.GetPosts{}
	for _, post := range posts {
		temp := &pb.GetPostInfo{
			Id:          post.Id,
			Title:       post.Title,
			Description: post.Description,
			Content:     post.Content,
			UpdatedAt:   post.UpdatedAt,
			CreatedAt:   post.CreatedAt,
		}
		temp.Medias, err = p.Storage.Post().GetMediasByPostId(pb.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting medias of posts", logger.Error(err))
			// return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get all medias of the posts")

		}
		owner, err := p.Client.Customer().GetCustomerI(ctx, &customer.Id{Id: post.OwnerId})
		if err != nil {
			p.Logger.Error("Error while getting owner of posts", logger.Error(err))
			return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get user of the posts")
		}
		tempOwner := &pb.Customer{
			Id:          owner.Id,
			FirstName:   owner.FirstName,
			LastName:    owner.LastName,
			Bio:         owner.Bio,
			Email:       owner.Email,
			PhoneNumber: owner.PhoneNumber,
			UpdatedAt:   owner.UpdatedAt,
			CreatedAt:   owner.CreatedAt,
		}
		// fmt.Println(owner)
		for _, ad := range owner.Addresses {
			tempOwner.Addresses = append(tempOwner.Addresses, &pb.Address{
				OwnerId:    ad.OwnerId,
				District:   ad.District,
				Street:     ad.Street,
				HomeNumber: ad.HomeNumber,
				CreatedAt:  ad.CreatedAt,
				Id:         ad.Id,
				UpdatedAt:  ad.UpdatedAt,
			})
		}
		temp.Owner = tempOwner
		rewiews, err := p.Client.Rewiew().GetRewiewByPostId(ctx, &rewiew.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting post rewiews", logger.Error(err))
			// return nil, status.Error(codes.Internal, "Couldn't get the post")
			rewiews = &rewiew.GetRewiewsRes{}
		}
		for _, rew := range rewiews.Rewiews {
			temp.Rewiews = append(temp.Rewiews, &pb.RewiewRes{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
				CreatedAt:   rew.CreatedAt,
				UpdatedAt:   rew.UpdatedAt,
			})
		}
		temp.AvarageRewiew = uint32(rewiews.Rating)
		response.Posts = append(response.Posts, temp)
	}
	response.Count = int64(len(posts))
	return response, nil
}
