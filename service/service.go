package service

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/post-service/pkg/logger"
	grpcclient "gitlab.com/exam23/post-service/service/grpcClient"
	"gitlab.com/exam23/post-service/storage"
)

type PostService struct {
	Storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Cleints
}

func NewPostService(db *sqlx.DB, l logger.Logger, client grpcclient.Cleints) *PostService {
	return &PostService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
		Client: client,
	}
}


