package service

import (
	"context"

	"gitlab.com/exam23/post-service/genproto/customer"
	pb "gitlab.com/exam23/post-service/genproto/post"
	"gitlab.com/exam23/post-service/genproto/rewiew"
	"gitlab.com/exam23/post-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (p *PostService) Create(ctx context.Context, req *pb.CreatePost) (*pb.Post, error) {
	res, err := p.Storage.Post().Create(req)
	if err != nil {
		p.Logger.Error("Error while creating post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't create your post, try again")
	}

	return res, nil
}

func (p *PostService) Update(ctx context.Context, req *pb.Post) (*pb.Post, error) {
	res, err := p.Storage.Post().Update(req)
	if err != nil {
		p.Logger.Error("Error while Updating post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't update your post, try again")
	}

	return res, nil
}

func (p *PostService) Delete(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := p.Storage.Post().Delete(req)
	if err != nil {
		p.Logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	_, err = p.Client.Rewiew().DeleteByPostId(ctx, &rewiew.Id{Id: req.Id})
	if err != nil {
		p.Logger.Error("Error while Deleting post rewiews", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post rewiews")
	}
	return &pb.Empty{}, nil
}

func (p *PostService) DeleteByOwnerId(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	pIds, err := p.Storage.Post().DeleteByOwnerID(req)
	if err != nil {
		p.Logger.Error("Error while Deleting post by owner id", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	for _, id := range pIds {
		_, err := p.Client.Rewiew().DeleteByPostId(ctx, &rewiew.Id{Id: id})
		if err != nil {
			p.Logger.Error("Error while Deleting rewiews by post id", logger.Error(err))
			return nil, status.Error(codes.Internal, "Couldn't delete your post rewiews")
		}
	}
	return &pb.Empty{}, nil
}

func (p *PostService) GetById(ctx context.Context, req *pb.Id) (*pb.GetPostInfo, error) {
	res, err := p.Storage.Post().GetById(req)
	if err != nil {
		p.Logger.Error("Error while getting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't get the post")
	}
	response := &pb.GetPostInfo{
		Id:          res.Id,
		Title:       res.Title,
		Description: res.Description,
		Content:     res.Content,
		Medias:      res.Medias,
		CreatedAt:   res.CreatedAt,
		UpdatedAt:   res.UpdatedAt,
	}

	// Need grpc connection with rewiew service
	rewiews, err := p.Client.Rewiew().GetRewiewByPostId(ctx, &rewiew.Id{Id: response.Id})
	if err != nil {
		p.Logger.Error("Error while getting post rewiews", logger.Error(err))
		// return nil, status.Error(codes.Internal, "Couldn't get the post")
		rewiews = &rewiew.GetRewiewsRes{}
	}
	for _, rew := range rewiews.Rewiews {
		response.Rewiews = append(response.Rewiews, &pb.RewiewRes{
			Id:          rew.Id,
			PostId:      rew.PostId,
			CustomerId:  rew.CustomerId,
			Description: rew.Description,
			Rating:      rew.Rating,
			CreatedAt:   rew.CreatedAt,
			UpdatedAt:   rew.UpdatedAt,
		})
	}
	response.AvarageRewiew = uint32(rewiews.Rating)
	owner, err := p.Client.Customer().GetCustomerI(ctx, &customer.Id{
		Id: res.OwnerId,
	})
	if err != nil {
		p.Logger.Error("Error while getting post owner", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't get the post owner")
	}
	tempOwner := &pb.Customer{
		Id:          owner.Id,
		FirstName:   owner.FirstName,
		LastName:    owner.LastName,
		Bio:         owner.Bio,
		Email:       owner.Email,
		PhoneNumber: owner.PhoneNumber,
		CreatedAt:   owner.CreatedAt,
		UpdatedAt:   owner.UpdatedAt,
	}
	for _, ad := range owner.Addresses {
		tempOwner.Addresses = append(tempOwner.Addresses, &pb.Address{
			Id:         ad.Id,
			OwnerId:    ad.OwnerId,
			District:   ad.District,
			Street:     ad.Street,
			HomeNumber: ad.HomeNumber,
			CreatedAt:  ad.CreatedAt,
			UpdatedAt:  ad.UpdatedAt,
		})
	}
	response.Owner = tempOwner
	return response, nil
}

func (p *PostService) GetByOwnerId(ctx context.Context, req *pb.Id) (*pb.GetPosts, error) {
	res, err := p.Storage.Post().GetByOwnerId(req)
	response := &pb.GetPosts{}
	if err != nil {
		p.Logger.Error("Error while getting posts", logger.Error(err))
		return &pb.GetPosts{}, status.Error(codes.Internal, "Couldn't get the posts")
	}
	response.Count = int64(len(res))
	for _, r := range res {
		temp := &pb.GetPostInfo{
			Id:          r.Id,
			Title:       r.Title,
			Description: r.Description,
			Content:     r.Content,
			Medias:      r.Medias,
			CreatedAt:   r.CreatedAt,
			UpdatedAt:   r.UpdatedAt,
		}
		// customer connection needed to get owner info
		// customer, err :=

		rewiews, err := p.Client.Rewiew().GetRewiewByPostId(ctx, &rewiew.Id{Id: temp.Id})
		if err != nil {
			p.Logger.Error("Error while getting post rewiews", logger.Error(err))
			// return nil, status.Error(codes.Internal, "Couldn't get the post")
			rewiews = &rewiew.GetRewiewsRes{}
		}
		for _, rew := range rewiews.Rewiews {
			temp.Rewiews = append(temp.Rewiews, &pb.RewiewRes{
				Id:          rew.Id,
				PostId:      rew.PostId,
				CustomerId:  rew.CustomerId,
				Description: rew.Description,
				Rating:      rew.Rating,
				UpdatedAt:   rew.UpdatedAt,
				CreatedAt:   rew.CreatedAt,
			})
		}
		temp.AvarageRewiew = uint32(rewiews.Rating)
		response.Posts = append(response.Posts, temp)
	}
	return response, nil
}
