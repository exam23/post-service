package grpcclient

import (
	"fmt"

	"gitlab.com/exam23/post-service/config"
	cs "gitlab.com/exam23/post-service/genproto/customer"
	rs "gitlab.com/exam23/post-service/genproto/rewiew"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Cleints interface {
	Rewiew() rs.RewiewServiceClient
	Customer() cs.CustomerServiceClient
}
type ServiceManager struct {
	config          config.Config
	rewiewService   rs.RewiewServiceClient
	customerService cs.CustomerServiceClient
}

func New(c config.Config) (Cleints, error) {
	rewiew, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.RewiewServiceHost, c.RewiewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &ServiceManager{}, nil
	}
	customer, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.CustomerServiceHost, c.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &ServiceManager{}, nil
	}

	return &ServiceManager{
		config:          c,
		rewiewService:   rs.NewRewiewServiceClient(rewiew),
		customerService: cs.NewCustomerServiceClient(customer),
	}, nil
}

func (s *ServiceManager) Rewiew() rs.RewiewServiceClient {
	return s.rewiewService
}

func (s *ServiceManager) Customer() cs.CustomerServiceClient {
	return s.customerService
}
