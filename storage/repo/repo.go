package repo

import (
	pb "gitlab.com/exam23/post-service/genproto/post"
)

type PostStorageI interface {
	Create(req *pb.CreatePost) (*pb.Post, error)
	Update(req *pb.Post) (*pb.Post, error)
	Delete(req *pb.Id) error
	DeleteByOwnerID(req *pb.Id) ([]string, error)
	GetById(req *pb.Id) (*pb.Post, error)
	GetByOwnerId(req *pb.Id) ([]*pb.Post, error)
	GetAll() ([]*pb.Post, error)
	// media
	GetMediasByPostId(postId pb.Id) ([]*pb.Media, error)
	DeleteMedia(postId pb.Id) error
	CreatMedia(media *pb.Media) (*pb.Media, error)
	GetPostBySearchAndOrder(pb.GetListBySearchReq) ([]*pb.Post, error)
}
