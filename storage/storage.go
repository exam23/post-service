package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/post-service/storage/postgres"
	"gitlab.com/exam23/post-service/storage/repo"
)

type IStorage interface {
	Post() repo.PostStorageI
}

type StoragePg struct {
	Db       *sqlx.DB
	userRepo repo.PostStorageI
}

// NewStoragePg
func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		userRepo: postgres.NewPostRepo(db),
	}
}

func (s StoragePg) Post() repo.PostStorageI {
	return s.userRepo
}
