package postgres

import (
	pb "gitlab.com/exam23/post-service/genproto/post"
)

func (p *PostRepo) Create(req *pb.CreatePost) (*pb.Post, error) {
	response := &pb.Post{}
	err := p.Db.QueryRow(`
		INSERT INTO  posts (
			owner_id, 
			title,
			description,
			content
		)
		values($1, $2, $3, $4) returning 
		id,
		owner_id, 
		title,
		description,
		content, 
		created_at,
		updated_at
	`,
		req.OwnerId,
		req.Title,
		req.Description,
		req.Content).Scan(
		&response.Id,
		&response.OwnerId,
		&response.Title,
		&response.Description,
		&response.Content,
		&response.CreatedAt,
		&response.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	for _, media := range req.Medias {
		media.PostId = response.Id
		media, err = p.CreatMedia(media)
		if err != nil {
			return nil, err
		}
		response.Medias = append(response.Medias, media)
	}
	return response, err
}

func (p *PostRepo) Update(req *pb.Post) (*pb.Post, error) {
	err := p.Db.QueryRow(`UPDATE posts SET 
		owner_id=$1, 
		title=$2,
		description=$3,
		content=$4,
		updated_at=NOW()
		WHERE id=$5 AND deleted_at IS NULL
	`).Err()
	if err != nil {
		return nil, err
	}

	medias := []*pb.Media{}
	for _, media := range req.Medias {
		media.PostId = req.Id
		m, err := p.UpdateMedia(media)
		if err != nil {
			return nil, err
		}
		medias = append(medias, m)
	}
	req.Medias = medias
	return req, nil
}

func (p *PostRepo) Delete(req *pb.Id) error {
	return p.Db.QueryRow(`
		update posts SET deleted_at=NOW() where id=$1 and deleted_at is null
	`, req.Id).Err()
}

func (p *PostRepo) DeleteByOwnerID(req *pb.Id) ([]string, error) {
	rows, err := p.Db.Query(`
		update posts SET deleted_at=NOW() where owner_id=$1 and deleted_at is null returning id
	`, req.Id)
	if err != nil {
		return []string{}, err
	}
	defer rows.Close()
	res := []string{}
	for rows.Next() {
		temp := ""
		rows.Scan(&temp)
		res = append(res, temp)
	}
	return res, nil
}

func (p *PostRepo) GetById(req *pb.Id) (*pb.Post, error) {
	response := &pb.Post{}
	err := p.Db.QueryRow(`SELECT 
		id,
		owner_id, 
		title,
		description,
		content, 
		created_at, 
		updated_at
		FROM posts
		where id=$1 and deleted_at is null
	`, req.Id).Scan(
		&response.Id,
		&response.OwnerId,
		&response.Title,
		&response.Description,
		&response.Content,
		&response.CreatedAt,
		&response.UpdatedAt,
	)
	if err != nil {
		return &pb.Post{}, err
	}

	response.Medias, err = p.GetMediasByPostId(pb.Id{Id: response.Id})
	if err != nil {
		return &pb.Post{}, err
	}

	return response, nil
}

func (p *PostRepo) GetByOwnerId(req *pb.Id) ([]*pb.Post, error) {
	rows, err := p.Db.Query(`SELECT
		id,
		owner_id, 
		title,
		description,
		content,
		created_at, 
		updated_at
		FROM posts
		where owner_id=$1 and deleted_at is null`, req.Id)
	if err != nil {
		return []*pb.Post{}, err
	}
	defer rows.Close()

	response := []*pb.Post{}

	for rows.Next() {
		temp := &pb.Post{}
		err = rows.Scan(
			&temp.Id,
			&temp.OwnerId,
			&temp.Title,
			&temp.Description,
			&temp.Content,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			return []*pb.Post{}, err
		}
		temp.Medias, err = p.GetMediasByPostId(pb.Id{Id: temp.Id})
		if err != nil {
			temp.Medias = nil
		}
		response = append(response, temp)
	}
	return response, nil
}
