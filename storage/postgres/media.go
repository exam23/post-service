package postgres

import pb "gitlab.com/exam23/post-service/genproto/post"

func (p *PostRepo) CreatMedia(media *pb.Media) (*pb.Media, error) {
	res := &pb.Media{}
	err := p.Db.QueryRow(`
			INSERT INTO medias( 
				post_id, 
				name_m, 
				link, 
				type_m
			) 
			values($1, $2, $3, $4)
			returning 
			id,
			post_id, 
			name_m, 
			link, 
			type_m,
			created_at,
			updated_at
		`, media.PostId, media.Name, media.Link, media.Type).Scan(
		&res.Id,
		&res.PostId,
		&res.Name,
		&res.Link,
		&res.Type,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	return res, err
}

func (p *PostRepo) DeleteMedia(postId pb.Id) error {
	return p.Db.QueryRow(`update medias SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, postId.Id).Err()
}

func (p *PostRepo) GetMediasByPostId(postId pb.Id) ([]*pb.Media, error) {
	rows, err := p.Db.Query(`SELECT 
	id,
	post_id, 
	name_m, 
	link, 
	type_m, 
	created_at,
	updated_at
	FROM medias where post_id=$1 and deleted_at is null`, postId.Id)
	if err != nil {
		return []*pb.Media{}, err
	}
	defer rows.Close()

	response := []*pb.Media{}
	for rows.Next() {
		temp := &pb.Media{}
		err = rows.Scan(
			&temp.Id,
			&temp.PostId,
			&temp.Name,
			&temp.Link,
			&temp.Type,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			return []*pb.Media{}, err
		}
		response = append(response, temp)
	}

	return response, nil
}

func (p *PostRepo) UpdateMedia(media *pb.Media) (*pb.Media, error) {
	err := p.Db.QueryRow(`
		update medias SET 
		updated_at=NOW(),
		post_id=$1, 
		name_m=$2, 
		link=$3, 
		type_m=$4
		WHERE id=$5 and deleted_at is null
		returning updated_at
	`).Scan(&media.UpdatedAt)
	return media, err
}
