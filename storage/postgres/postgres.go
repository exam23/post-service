package postgres

import (
	"github.com/jmoiron/sqlx"
)

type PostRepo struct {
	Db *sqlx.DB
}

func NewPostRepo(db *sqlx.DB) *PostRepo {
	return &PostRepo{Db: db}
}