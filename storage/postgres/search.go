package postgres

import (
	"fmt"

	pb "gitlab.com/exam23/post-service/genproto/post"
)

func (r *PostRepo) GetAll() ([]*pb.Post, error) {
	rows, err := r.Db.Query(`SELECT 
	id,
	owner_id, 
	title,
	description,
	content,
	created_at, 
	updated_at
	FROM posts
	where deleted_at is null`)
	if err != nil {
		return []*pb.Post{}, err
	}
	defer rows.Close()
	response := []*pb.Post{}
	for rows.Next() {
		temp := &pb.Post{}
		err = rows.Scan(
			&temp.Id,
			&temp.OwnerId,
			&temp.Title,
			&temp.Description,
			&temp.Content,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			return response, err
		}

		response = append(response, temp)
	}
	return response, nil
}

func (r *PostRepo) GetPostBySearchAndOrder(req pb.GetListBySearchReq) ([]*pb.Post, error) {
	offset := (req.Page - 1) * req.Limit	
	search := fmt.Sprintf("%s LIKE $1", req.Search.Field)
	order := fmt.Sprintf("ORDER BY %s %s", req.Orders.Field, req.Orders.Value)

	query := `SELECT 
	id,
	owner_id, 
	title,
	description,
	content,
	created_at, 
	updated_at
	FROM posts
	where deleted_at is null` + " and " + search + " " + order + " "

	rows, err := r.Db.Query(query+"LIMIT $2 OFFSET $3", "%" + req.Search.Value + "%", req.Limit, offset)
	if err != nil {
		return []*pb.Post{}, err
	}
	defer rows.Close()
	response := []*pb.Post{}

	for rows.Next() {
		temp := &pb.Post{}
		err = rows.Scan(
			&temp.Id,
			&temp.OwnerId,
			&temp.Title,
			&temp.Description,
			&temp.Content,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			return response, err
		}
		response = append(response, temp)
	} 
	return response, nil
}
