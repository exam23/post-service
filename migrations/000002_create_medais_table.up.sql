CREATE TABLE IF NOT EXISTS medias (
    post_id UUID NOT NULL REFERENCES posts(id), 
    name_m TEXT NOT NULL,
    link TEXT NOT NULL,
    type_m VARCHAR(50) NOT NULL, 
    created_at TIME NOT NULL DEFAULT NOW(), 
    updated_at TIME NOT NULL DEFAULT NOW(),
    deleted_at TIME
);