ALTER TABLE posts DROP COLUMN created_at;
ALTER TABLE posts DROP COLUMN updated_at;
ALTER TABLE posts DROP COLUMN deleted_at;

ALTER TABLE posts ADD COLUMN created_at TIMESTAMPTZ;
ALTER TABLE posts ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE posts ADD COLUMN updated_at TIMESTAMPTZ;
ALTER TABLE posts ALTER COLUMN updated_at SET DEFAULT now();

ALTER TABLE posts ADD COLUMN deleted_at TIMESTAMPTZ;