package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment         string
	PostgresHost        string
	PostgresPort        int
	PostgresDatabase    string
	PostgresUser        string
	PostgresPassword    string
	LogLevel            string
	RPCPort             string
	RewiewServiceHost   string
	RewiewServicePort   int
	CustomerServiceHost string
	CustomerServicePort int
}

// Load loads environment variables and inflates Config
func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "post_service"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.RewiewServiceHost = cast.ToString(GetOrReturnDefault("REWIEW_SERVICE_RPC_HOST", "localhost"))
	c.RewiewServicePort = cast.ToInt(GetOrReturnDefault("REWIEW_SERVICE_RPC_PORT", "8000"))

	c.CustomerServiceHost = cast.ToString(GetOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToInt(GetOrReturnDefault("CUSTOMER_SERVICE_PORT", 7070))
	c.RPCPort = cast.ToString(GetOrReturnDefault("POST_SERVICE_RPC_PORT", ":9000"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
